"""
Author: Adam Bedard

Test Cases for cve download.

Run:
cd <path>/cloudpassage/test/unit
$ py.test cve_download_test.py

Notes:

On the day of your evaluation of this Test file - if you find this test
does not pass because it cannot find the hard coded cve, please change the
self.specific_cve variable to a valid cve within the nvdcve-2.0-Recent.xml file.

I found through my testing that the cve numbers in the nvdcve-2.0-Recent.xml.gz
change. So I am a little worried that this script will be evaluated on a day that the
cve numbers have changed and the "test_if_xml_entries_contains_a_specific_cve" will fail
( which might be a good thing )

Challenge Requirements:

1) As part of a setup method, automate the download of
http://static.nvd.nist.gov/feeds/xml/cve/nvdcve-2.0-Recent.xml.gz

2)  Create test methods for the following:
    a. a specific CVE is included (you choose, hardcode into test).

    b. the referenced CVE has at least one cpe defined
    ( this is too vague so I am going to make an assumption which is for
    a cpe-lang top level xml tag - check for at least one sub cpe-lang xml tag to exist )

"""

import gzip
import os.path
import pytest
import sys
import urllib

from xml.dom import minidom

class TestCve():
    """
    This class defines the test suite which implements the requirements of the challenge.

    I've added some additional test cases for sanity checking - to make sure things are where they
    are supposed to be, before applying the tests for the requirements of the challenge.

    I've made the following assumptions:
    1. That the name of the cve will always be the same.
    2. That url to download the gzip file will always be the same.
    3. That there is a "files" folder located two directories above
    4. The files directory will never be moved.
    5. The xml tags will never change i.e. entry, cpe-lang:logical-test, and cpe-lang:fact-ref

    1. Define global variables to use throughout the test cases.
    2. Enter try block at the point of downloading the gzip file.

    """
    file_dir = "../../files"
    filename = "../../files/nvdcve-2.0-Recent.xml.gz"
    specific_cve = "CVE-2014-8873"
    url = "http://static.nvd.nist.gov/feeds/xml/cve/nvdcve-2.0-Recent.xml.gz"
    xml_file = "../../files/nvdcve-2.0-Recent.xml"
    try:
        if not os.path.exists(file_dir):
            os.makedirs(file_dir)
        urllib.urlretrieve(url=url, filename=filename)
        with gzip.open(filename, 'rb') as afile:
            file_contents = afile.read()
        afile.close()
        axmlfile = open(xml_file, 'w')
        axmlfile.write(file_contents)
        axmlfile.close()
    except IOError as exception:
        print "I/O error ({0}): {1}".format(exception.errno, exception.strerror)
    except:
        print "Unexpected error:", sys.exc_info()[0]
        raise

    def tearDown(self):
        """
        1. Derefernce global vars.
        """
        self.specific_cve = None
        self.url = None
        self.filename = None
        self.xml_file = None

    def test_if_gz_file_was_successfully_downloaded(self):
        """
        1. Check to see if file was downloaded.
        """
        result = os.path.isfile(self.filename)
        assert result is True

    def test_if_xml_file_was_successfully_created(self):
        """
        1. Check to see if the xml file was successfully created.
        """
        result = os.path.isfile(self.xml_file)
        assert result is True

    def test_if_xml_contains_entries(self):
        """
        1. Check if the xml file has the entry xml tag - it makes sense to check for this first.
        """
        xmldoc = minidom.parse(self.xml_file)
        itemlist = xmldoc.getElementsByTagName('entry')
        assert len(itemlist) > 0

    def test_if_xml_entries_contains_a_specific_cve(self):
        """
        1. Test for the specified xml tag - Part 2 of challenge item a.
        """
        xmldoc = minidom.parse(self.xml_file)
        itemlist = xmldoc.getElementsByTagName('entry')
        cve_list = []
        for item in itemlist:
            cve_list.append(item.attributes['id'].value)
        assert self.specific_cve in cve_list

    def test_if_cve_has_at_least_one_cpe_defined_for_specific_cve(self):
        """
         1. Test that for the specified cve that it has at least on cpe.
         2. The wording in Part 2, item b was kind of vague, so I made and assumption
         3. My assumption was that if there exist a "cpe-lang:logical-test" that it has at least
            one "cpe-lang:fact-ref"
        """
        xmldoc = minidom.parse(self.xml_file)
        itemlist = xmldoc.getElementsByTagName('entry')
        for entry in itemlist:
            if entry.attributes['id'].value == self.specific_cve:
                cpe_logic_test = entry.getElementsByTagName('cpe-lang:logical-test')
                if len(cpe_logic_test) > 0:
                    cpe_lang_fact_ref = cpe_logic_test[0].getElementsByTagName('cpe-lang:fact-ref')
                    assert len(cpe_lang_fact_ref) > 0

    def test_if_cve_has_xss_attacks_within_xml_file(self):
        """
        1. Test the string of the xml file for known xss patterns.
        2. This would catch stored xss attacks.
        3. However no real hacker uses XSS so this would be modified to
           something useful.
        """
        xss_blacklist = [
            "javascript:alert('XSS')",
            "JaVaScRiPt:alert('XSS')",
            "onmouseover=alert(document.cookie)",
            "javascript:alert(String.fromCharCode(88,83,83))",
            "onmouseover=alert('xxs')",
            "onerror=javascript:alert('XSS');",
            "jav&#x09;ascript:alert('XSS');",
            "onerror=\"jav&#x09;ascript:alert('XSS');"
        ]
        for xss in xss_blacklist:
            if xss in gzip.open(self.filename, 'rb').read():
                pytest.fail("Xss pattern found in file")

    def test_if_mysql_injection_witin_xml_file(self):
        """
        1. Test the string of the xml file for known mysql injection patterns
        2. You may not think this is realistic but it would catch stored
           mysql injections attacks coming from the source that generates these files.
        """
        mysql_injection_blacklist = [
            'or 1=1',
            'or 1=1 --',
            "1' or '1' = '1",
            "1' or '1' = '1"
            "1' or '1' = '1'))/*",
            "1'%20or%20'1'%20=%20'1'))/*",
            "1 /*! and 1=0 */",
            "1' or '1' = '1'))",
            "1 AND 1=0 UNION SELECT USER()",
            "1 AND USER() like 'root%'",
         ]
        for msqlin in mysql_injection_blacklist:
            if msqlin in gzip.open(self.filename, 'rb').read():
                pytest.fail("Mysql injection pattern found in file")

if __name__ == '__main__':
    pass
