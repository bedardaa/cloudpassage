"""
Author: Adam Bedard

Test Cases for file integrity policy.

Run:
cd <path>/cloudpassage/test/unit
$ py.test file_integrity_test.py

Challenge Requirements:

1. Please download (manually or automated as part of the test suite) at least one
CloudPassage Configuration security management or File integrity monitoring template policy.
Will require signing up for a free account
:https://portal.cloudpassage.com/registrations/new
Make sure you preface your customer account name with "candidate-"
(only so you don't fall into our marketing machine which sends various emails to corporate accounts)

2. Create test methods for the following:
    a. platform is not nil
    b. platform is either linux or windows
    c. each rule "alert" attribute is either true or false

"""
import json
import os.path
import sys
import pytest

class TestFileIntegrityPolicy():
    """
    This class defines the test suite which implements the requirements of the challenge.

    I've added some additional test cases for sanity checking - to make sure things
    are where they are supposed to be, before applying the tests for the requirements
    of the challenge.

    I've made the following assumptions:
    1. That there is a "files" folder located two directories above
    2. The files directory will never be moved.
    """
    file_dir = "../../files"
    file_name = '../../files/core-registry-keys-windows-2008-v1.fim.json'
    try:
        if not os.path.exists(file_dir):
            os.makedirs(file_dir)

        if not os.path.isfile(file_name):
            raise "The policy json file is missing from the files directory - Aborting Test"

        file_policy = open(file_name).read()
        policy = json.loads(file_policy)
    except IOError as exception:
        print "I/O error ({0}): {1}".format(exception.errno, exception.strerror)
    except:
        print "Unexpected error:", sys.exc_info()[0]
        raise

    def tearDown(self):
        """
        1. Derefernce global vars.
        """
        self.file_policy = None
        self.policy = None

    def test_policy_is_a_python_dictionary(self):
        """
        1. Test that the policy var is a python dictionary to ensure the other
        tests will correctly function.
        """
        result = isinstance(self.policy, dict)
        assert result is True

    def test_policy_is_not_an_empty_python_dictionary(self):
        """
        1. Test the that the python dictionary is not empty to ensure the other
        tests will correctly function.
        """
        assert self.policy is not None

    def test_if_fim_policy_key_exists_in_python_dictionary(self):
        """
        1. Test that we have a fim_policy from which we can grab a "platform".
        Ensuring the other tests will function.
        """
        assert 'fim_policy' in self.policy.keys()

    def test_if_platform_key_exists_in_python_dictionary(self):
        """
        1. Test that we have a "platform" key.
        """
        k = self.policy['fim_policy']
        assert 'platform' in k.keys()

    def test_platform_is_not_nil(self):
        """
        1. Test that the platform key is not None ( or nil )
        2. This is "a" in part 2 of the challenge.
        """
        platform = self.policy['fim_policy']['platform']
        assert platform is not None

    def test_platform_is_either_linux_or_windows(self):
        """
        1. Test that the platform key can only be either windows or linux
        2. This is "b" in part 2 of the challenge.
        """
        valid_platforms = ['windows', 'linux']
        platform = self.policy['fim_policy']['platform']
        assert platform in valid_platforms

    def test_each_rules_alert_attribute_is_true_or_false(self):
        """
        1. Test that for all alert rules, they can only be False or True
        2. This is "c" in part 2 of the challenge.
        """
        valid_alert_rules = [False, True]
        alert_rules = self.policy['fim_policy']['rules']
        for rule in alert_rules:
            assert rule['alert'] in valid_alert_rules

    def test_if_json_has_xss_attacks_within_xml_file(self):
        """
        1. Test the string of the xml file for known xss patterns.
        2. This would catch stored xss attacks.
        3. However no real hacker uses XSS so this would be modified to
           something useful.
        """
        xss_blacklist = [
            "javascript:alert('XSS')",
            "JaVaScRiPt:alert('XSS')",
            "onmouseover=alert(document.cookie)",
            "javascript:alert(String.fromCharCode(88,83,83))",
            "onmouseover=alert('xxs')",
            "onerror=javascript:alert('XSS');",
            "jav&#x09;ascript:alert('XSS');",
            "onerror=\"jav&#x09;ascript:alert('XSS');"
        ]
        for xss in xss_blacklist:
            if xss in open(self.file_name).read():
                pytest.fail("Xss pattern found in file")

    def test_if_mysql_injection_witin_json_file(self):
        """
        1. Test the string of the xml file for known mysql injection patterns
        2. You may not think this is realistic but it would catch stored
           mysql injections attacks coming from the source that generates these files.
        """
        mysql_injection_blacklist = [
            'or 1=1',
            'or 1=1 --',
            "1' or '1' = '1",
            "1' or '1' = '1"
            "1' or '1' = '1'))/*",
            "1'%20or%20'1'%20=%20'1'))/*",
            "1 /*! and 1=0 */",
            "1' or '1' = '1'))",
            "1 AND 1=0 UNION SELECT USER()",
            "1 AND USER() like 'root%'",
         ]
        for msqlin in mysql_injection_blacklist:
            if msqlin in open(self.file_name).read():
                pytest.fail("Mysql injection pattern found in file")

if __name__ == '__main__':
    pass
